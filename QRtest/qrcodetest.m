clear;
close all;
I1rgb = imread('qrcode.png');
I2rgb = imread('qrcode5.jpg');
I1gs = rgb2gray(I1rgb);
I2gs = rgb2gray(I2rgb);
original = rgb2gray(I1rgb);
distorted = rgb2gray(I2rgb);

% info = imfinfo('qrcode5.jpg');
% info.BitDepth

%--------------------------------------------
% points1 = detectSURFFeatures(I1gs);
% points2 = detectSURFFeatures(I2gs);
% [features1, validPoints1] = extractFeatures(I1gs, points1);
% [features2, validPoints2] = extractFeatures(I2gs, points2);
% 
% indexPairs = matchFeatures(features1, features2);
% matchedPoints1 = validPoints1(indexPairs(:,1));
% matchedPoints2 = validPoints2(indexPairs(:,2)); 
% showMatchedFeatures(I1gs, I2gs, matchedPoints1, matchedPoints2, 'montage')
%-------------------------------------------------------
ptsOriginal = detectSURFFeatures(original);
ptsDistorted = detectSURFFeatures(distorted);
[featuresOriginal, validPtsOriginal] = extractFeatures(original, ptsOriginal);
[featuresDistorted, validPtsDistorted] = extractFeatures(distorted, ptsDistorted);
indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
matchedOriginal = validPtsOriginal(indexPairs(:,1));
matchedDistorted = validPtsDistorted(indexPairs(:,2));
figure;
showMatchedFeatures(original,distorted,matchedOriginal,matchedDistorted, 'montage');

[tform, inlierDistorted, inlierOriginal] = estimateGeometricTransform(matchedDistorted,...
matchedOriginal, 'similarity');

figure;
showMatchedFeatures(original,distorted,inlierOriginal,inlierDistorted);
title('Matching points (inliers only)');
legend('ptsOriginal','ptsDistorted');

Tinv = tform.invert.T;
ss = Tinv(2,1);
sc = Tinv(1,1);
scaleRecovered = sqrt(ss*ss + sc*sc)
thetaRecovered = atan2(ss,sc)*180/pi

outputView = imref2d(size(original));
recovered = imwarp(distorted,tform,'OutputView',outputView);
figure, imshowpair(original,recovered,'montage')

rpy = tr2rpy(tform.T)
angle=rad2deg(rpy)

% I1single = single(I1gs);
% I2single = single(I2gs);
% 
% [f1, d1] = vl_sift(I1single);
% 
% imshow(I1gs);
% hold on
% h1 = vl_plotframe(f1);
% h2 = vl_plotframe(f1);
% set(h1,'color','k','linewidth',3) ;
% set(h1,'color','y','linewidth',2) ;
% 
% [f2, d2] = vl_sift(I2single);
% [matches, scores] = vl_ucbmatch(da, db);
% 
% showMatchedFeatures(I1gs, I2gs, f1(1:2, matches(1,:))', f2(1:2, matches(2,:))', 'montage');