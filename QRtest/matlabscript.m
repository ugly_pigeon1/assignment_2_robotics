cute_enable_robot_client = rossvcclient('enableCyton');
cute_enable_robot_msg = rosmessage(cute_enable_robot_client);
cute_enable_robot_msg.TorqueEnable = true; %false
%cute_enable_robot_msg.TorqueEnable = false; %false
cute_enable_robot_client.call(cute_enable_robot_msg);

%%
cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable')
cute_enable_gripper_msg = rosmessage(cute_enable_gripper_client);

cute_enable_gripper_msg.TorqueEnable = 1 %0
cute_enable_gripper_client.call(cute_enable_gripper_msg);
%%
stateSub = rossubscriber('/joint_states');
receive(stateSub,2)
msg = stateSub.LatestMessage;
msg.Position
%msg.JointAngles
%msg.Pos
%%

cute_move_client = rossvcclient('/GoToPosition');
cute_move_msg = rosmessage(cute_move_client);
%%
%cute_move_msg.JointStates = [1,1,1,1,1,1,1];
cute_move_msg.JointStates = [0,0,0,0,0,0,0];
%cute_move_msg.JointStates=[1,0,0,0,0,0,0];
cute_move_client.call(cute_move_msg);
%cute_move_client.delete
%%
cute_move_msg.JointStates = [0,0,0,0,0,1,0];
cute_move_client.call(cute_move_msg);
%%
%cute_enable_robot_client.call(cute_enable_robot_msg);
cute_move_msg.JointStates = [1,0,0,0,2,2,0];
cute_move_client.call(cute_move_msg);
%%
cute_move_msg.JointStates = [2,0,0.4,0,2,2,0];
cute_move_client.call(cute_move_msg);
% x
% cute_move_msg.JointStates = [0,0,0,0,1,1,0];
% cute_move_client.call(cute_move_msg);
% x
% cute_move_msg.JointStates = [1,0,0,0,0,0,0];
% cute_move_client.call(cute_move_msg);

%%
cute_single_joint_client = rossvcclient('/MoveSingleJoint');
cute_single_joint_msg = rosmessage(cute_single_joint_client);

cute_single_joint_msg.JointNumber = 1;% Joints 0-6
cute_single_joint_msg.Position = 0.1;% (Rads)
cute_single_joint_client.call(cute_single_joint_msg);

%%
cute_position_publisher = rospublisher('/cyton_position_commands');
cute_position_msg = rosmessage(cute_position_publisher);
%%
    cute_position_msg.Data = [1.5,0,0,0,0,2.5,0];
    cute_position_publisher.send(cute_position_msg);
    cute_position_msg.Data = [1.5,0,0,0,0,2.5,0];
    %%
    claw_client = rossvcclient('/ClawPosition');
claw_msg = rosmessage(claw_client);
%%
claw_msg.Data = 0; % Values must be between 0 (closed) and 26.5 (open) ~ a millimeter value of the claw open amount.
claw_client.call(claw_msg);
%%
claw_msg.Data = 26.5; % Values must be between 0 (closed) and 26.5 (open) ~ a millimeter value of the claw open amount.
claw_client.call(claw_msg);

%%
% robotics_cyton/Float64Request                                  
% robotics_cyton/Float64Response                                 
% robotics_cyton/TorqueEnableRequest                             
% robotics_cyton/TorqueEnableResponse                            
% robotics_cyton/cyton_enable_robotRequest                       
% robotics_cyton/cyton_enable_robotResponse                      
% robotics_cyton/cyton_move_all_jointsRequest                    
% robotics_cyton/cyton_move_all_jointsResponse                   
% robotics_cyton/cyton_move_single_jointRequest                  
% robotics_cyton/cyton_move_single_jointResponse 