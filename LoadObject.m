classdef LoadObject < handle
    %LoadObject Load various ply files and draw them
    %   Ply read is used to read vertex and face data, trisurf is used to
    %   plot the object data. Object data and plot as saved in different
    %   meber variable so they can be manipulated independently if need be.
    
    
    properties
        object; %The object plot
        %Ply file data
        faceData;
        vertexData;
        data;
        h;
    end
    
    methods
        function self = LoadObject(file_name, location)
            [self.faceData, self.vertexData,self.data] = plyread(file_name, 'tri');  
            self.object = trisurf(self.faceData,self.vertexData(:,1)+location(1,4),self.vertexData(:,2)+location(2,4),self.vertexData(:,3)+location(3,4));  
            drawnow();
        end
        function LoadObjectNoTR(file_name)
            [self.faceData, self.vertexData,self.data] = plyread(file_name, 'tri');  
            self.object = trisurf(self.faceData,self.vertexData(:,1),self.vertexData(:,2),self.vertexData(:,3));  
            drawnow();
        end
        
        
        
    end
end

