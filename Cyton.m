classdef Cyton < handle
    %Cyton Class for modelling and control of the robot
    %   Detailed explanation goes here
    %%
    properties
        % Robot model
        model;
        
        mainwaypoint = [1.5708,0.5001,-0.0752,0.7440,0.1043,1.7472,1.4711];
        servewaypoint = [-1.5708,0.5001,-0.0752,0.7440,0.1043,1.7472,1.4711];
        
        % Save the end effectors transform for logging
        endEffectorTr;
        %> workspace
        workspace = [-1 1 -1 1 -2 1];
        
        logger;
        
        qMatrix;
        
        CytonFlag;
        
        %Imported ply file data
        faceData;
        vertexData;
        data;
        
        %% Flags
        onedrinkflag;
        twodrinkflag;
        bothdrinkflag;
        hardcode;
        
        % QR DATA
        goalx1;
        goaly1;
        angle_qr3;
        goalx3;
        goaly3;
        angle_qr2;
        
        
        fixedPart;
        %> If we have a tool model which will replace the final links model, combined ply file of the tool model and the final link models
        toolModelFilename = []; % Available are: 'DabPrintNozzleTool.ply';
        toolParametersFilename = []; % Available are: 'DabPrintNozzleToolParameters.mat';
        cute_velocity_publisher
        cute_velocity_msg
        cute_single_joint_msg
        
        stateSub;
        
    end
    
    
    methods
        %% Constructor
        function self = Cyton(name, base)
            self.GetCytonRobot(name, base);
            PlotAndColourRobot(self);
            drawnow();
            self.logger = log4matlab(['logFileName_',self.model.name,'.log']);
        end
        
        %% GetUR3Robot
        % Given a name and location, create and return a UR3 robot model
        function GetCytonRobot(self, name, base)
            pause(0.001);
            
            L1 = Link('d',0.0628,   'a',0,      'alpha', pi/2,   'qlim',[deg2rad(-145) deg2rad(145)]);
            L2 = Link('d',0,        'a',0,      'alpha',-pi/2,   'qlim',[deg2rad(-100) deg2rad(100)]);
            L3 = Link('d',0.125657, 'a',0,      'alpha', pi/2,   'qlim',[deg2rad(-145) deg2rad(145)]);
            L4 = Link('d',0,        'a',0.06663,'alpha',-pi/2,   'qlim',[deg2rad(-100) deg2rad(100)],'offset',pi/2);
            L5 = Link('d',0,        'a',0.06663,'alpha', pi/2,   'qlim',[deg2rad(-90) deg2rad(90)]);
            L6 = Link('d',0,        'a',0,      'alpha',-pi/2,   'qlim',[deg2rad(-100) deg2rad(100)],'offset',-pi/2);
            L7 = Link('d',0.1388,   'a',0,      'alpha', 0,      'qlim',[deg2rad(-120) deg2rad(120)]);
            
            self.model = SerialLink([L1 L2 L3 L4 L5 L6 L7],'name',name);
            self.model.base = transl(base);
            %             self.model.plot(zeros(1,6),'workspace', self.workspace, 'scale', 0.5);
        end
        
        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available
        function PlotAndColourRobot(self)%robot,workspace)
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex + 1} ] = plyread(['CytonLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex + 1} = faceData;
                self.model.points{linkIndex + 1} = vertexData;
            end
            
            if ~isempty(self.toolModelFilename)
                [ faceData, vertexData, plyData{self.model.n + 1} ] = plyread(self.toolModelFilename,'tri');
                self.model.faces{self.model.n + 1} = faceData;
                self.model.points{self.model.n + 1} = vertexData;
                toolParameters = load(self.toolParametersFilename);
                self.model.tool = toolParameters.tool;
                self.model.qlim = toolParameters.qlim;
                warning('Please check the joint limits. They may be unsafe')
            end
            % Display robot
            self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end
            self.model.delay = 0;
            
            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
        %%
        function animateQ(self, qMatrix)
            for i=1:size(qMatrix)
                self.model.animate(qMatrix(i,:));
                pause(0.01);
            end
        end
        %%
        function initCyton(self)
            rosshutdown;
            rosinit('http://localhost:11311');
            disp('Connected to Local Host')
            self.stateSub = rossubscriber('/joint_states');
            cute_enable_robot_client = rossvcclient('enableCyton');
            cute_enable_robot_msg = rosmessage(cute_enable_robot_client);
            cute_enable_robot_msg.TorqueEnable = true; %false
            cute_enable_robot_client.call(cute_enable_robot_msg);
            cute_move_client = rossvcclient('goHome');
            cute_move_msg = rosmessage(cute_move_client);
            cute_move_client.call(cute_move_msg);
            cute_single_joint_client = rossvcclient('/MoveSingleJoint');
            cute_single_joint_msg = rosmessage(cute_single_joint_client);
            disp('Robot in starting Position, ready to begin')
            
        end
        %%
        function CytonOn(self)
            self.CytonFlag = 1;
            %             self.initCyton();
            
        end
        function CytonOff(self)
            self.CytonFlag = 0;
        end
        %%
        function moveCyton(self, goal)
            q1 = self.model.getpos();
            q2 = self.model.ikcon(goal, q1);
            steps = 50;
            s = lspb(0,1,steps);
            self.qMatrix = nan(steps,7);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [~, ~] = self.model.fkine(qMatrix(i,:));
                %self.model.animate(qMatrix(1,:));
                
            end
            collisionToEnvironment = self.checkcollsion(qMatrix)
            if collisionToEnvironment==0
                for MoveIndex = 1:size(qMatrix,1)
                    animate(self.model,qMatrix(MoveIndex,:));
                    drawnow();
                end
                self.commandCytonJoints(qMatrix);
            else
                disp("detect collsion");
            end
            
            %             self.logTransform(self.endEffectorTr);
        end
        %%
        function moveCytonVia(self, waypoint,goal)
            q1 = self.model.getpos();
            q2 = self.model.ikcon(waypoint, q1);
            steps = 50;
            s = lspb(0,1,steps);
            self.qMatrix = nan(steps,7);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [self.endEffectorTr, ~] = self.model.fkine(qMatrix(i,:));
                self.model.animate(qMatrix(1,:));
                
            end
            for MoveIndex = 1:size(qMatrix,wenca1)
                animate(self.model,qMatrix(MoveIndex,:));
                drawnow();
            end
            self.commandCytonJoints(qMatrix);
            fprintf('%s end effector is now at: \n',self.model.name);
            disp(self.endEffectorTr)
            q1 = self.model.getpos();
            q2 = self.model.ikcon(goal, q1);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [self.endEffectorTr, ~] = self.model.fkine(qMatrix(i,:));
                %self.model.animate(qMatrix(1,:));
                
            end
            for MoveIndex = 1:size(qMatrix,1)
                animate(self.model,qMatrix(MoveIndex,:));
                drawnow();
            end
            
            
            
            
            self.commandCytonJoints(qMatrix);
            
            %             self.logTransform(self.endEffectorTr);
        end
        function commandCytonJoints(self, qMatrix)
            if (self.CytonFlag == 1)
                a = size(qMatrix,1);
                %Move to a position
                cute_move_client = rossvcclient('/GoToPosition');
                cute_move_msg = rosmessage(cute_move_client);
                %                 for i=1:10:size(qMatrix)
                cute_move_msg.JointStates = qMatrix(a,:);
                cute_move_client.call(cute_move_msg);
                %                 pause(0.1);
                %                 end
                %                 input('press enter to continue')
            end
            
        end
        function gripper(self,d)
            if d>=0 && d <=26.5
                cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable');
                cute_enable_gripper_msg = rosmessage(cute_enable_gripper_client);
                cute_enable_gripper_msg.TorqueEnable = 1; %0
                cute_enable_gripper_client.call(cute_enable_gripper_msg);
                %%
                claw_client = rossvcclient('/ClawPosition');
                claw_msg = rosmessage(claw_client);
                
                
                claw_msg.Data = d; % Values must be between 0 (closed) and 26.5 (open) ~ a millimeter value of the claw open amount.
                claw_client.call(claw_msg);
            else
                disp('Enter gripper parameter between 0 and 26.5 mm')
            end
        end
        
        function goHome(self)
            cute_move_client = rossvcclient('goHome');
            cute_move_msg = rosmessage(cute_move_client);
            cute_move_client.call(cute_move_msg);
            disp('Robot in starting Position, ready to begin')
        end
        
        function moveRMRC(self, distanceDown, time,Endeffector_rotate_angle)
            self.initVel();
            pos = self.model.getpos();
            position = self.model.fkine(pos);
            t = time;             % Total time (s)
            deltaT = 0.02;      % Control frequency
            steps = t/deltaT;   % No. of steps for simulation
            delta = 2*pi/steps; % Small angle change
            epsilon = 0.1;      % Threshold value for manipulability/Damped Least Squares
            W = diag([1 1 1 0.1 0.1 0.1]);    % Weighting matrix for the velocity vector
            
            % 1.2) Allocate array data
            m = zeros(steps,1);             % Array for Measure of Manipulability
            qdot_test = zeros(steps,7);
            qMatrix = zeros(steps,7);       % Array for joint anglesR
            qdot = zeros(steps,7);          % Array for joint velocities
            theta = zeros(3,steps);         % Array for roll-pitch-yaw angles
            x = zeros(3,steps);             % Array for x-y-z trajectory
            positionError = zeros(3,steps); % For plotting trajectory error
            angleError = zeros(3,steps);    % For plotting trajectory error
            
            % 1.3) Set up trajectory, initial pose
            s = lspb(0,1,steps);                % Trapezoidal trajectory scalar
            %             x(1,:) = 0.5;
            %             x(2,:) = 0.5;
            delta = distanceDown/steps;
            for i=1:steps
                x(1,i) = position(1,4);   % Points in x
                x(2,i) = position(2,4); % Points in y
                x(3,i) = position(3,4)-i*delta;       % Points in z
                theta(1,i) = 0;                 % Roll angle
                theta(2,i) = pi;                 % Pitch angle
                theta(3,i) = deg2rad(Endeffector_rotate_angle);                 % Yaw angle
            end
            
            T = [rpy2r(theta(1,1),theta(2,1),theta(3,1)) x(:,1);zeros(1,3) 1];          % Create transformation of first point and angle
            %             q0 = zeros(1,7);                                                            % Initial guess for joint angles
            %            if i=1
            qMatrix(1,:)=pos;
            % 1.4) Track the trajectory with RMRC
            for i = 1:steps-1
                T = self.model.fkine(qMatrix(i,:));                                           % Get forward transformation at current joint state
                deltaX = x(:,i+1) - T(1:3,4);                                         	% Get position error from next waypoint
                Rd = rpy2r(theta(1,i+1),theta(2,i+1),theta(3,i+1));                     % Get next RPY angles, convert to rotation matrix
                Ra = T(1:3,1:3);                                                        % Current end-effector rotation matrix
                Rdot = (1/deltaT)*(Rd - Ra);                                                % Calculate rotation matrix error
                S = Rdot*Ra';                                                           % Skew symmetric!
                linear_velocity = (1/deltaT)*deltaX;
                angular_velocity = [S(3,2);S(1,3);S(2,1)];                              % Check the structure of Skew Symmetric matrix!!
                deltaTheta = tr2rpy(Rd*Ra');                                            % Convert rotation matrix to RPY angles
                xdot = W*[linear_velocity;angular_velocity];                          	% Calculate end-effector velocity to reach next waypoint.
                J = self.model.jacob0(qMatrix(i,:));                 % Get Jacobian at current joint state
                m(i) = sqrt(det(J*J'));
                if m(i) < epsilon  % If manipulability is less than given threshold
                    lambda = (1 - m(i)/epsilon)*5E-2;
                else
                    lambda = 0;
                end
                invJ = pinv(J'*J + lambda *eye(7))*J';                                   % DLS Inverse
                qdot(i,:) = (invJ*xdot)';                                                % Solve the RMRC equation (you may need to transpose the         vector)
                
                for j = 1:7                                                                % Loop through joints 1 to 6
                    if qMatrix(i,j) + deltaT*qdot(i,j) < self.model.qlim(j,1)                     % If next joint angle is lower than joint limit...
                        qdot(i,j) = 0; % Stop the motor
                    elseif qMatrix(i,j) + deltaT*qdot(i,j) > self.model.qlim(j,2)                 % If next joint angle is greater than joint limit ...
                        qdot(i,j) = 0; % Stop the motor
                    end
                end
                qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot(i,:);                         	% Update next joint state based on joint velocities
                
                qdot_line = qdot(i,:);
                scale = max(qdot_line);
                qdot_line =qdot_line/scale;
                % qdot_test = qdot_test(i,(qdot_line/scale));
                qdot_test(i,:)=qdot_line;
                positionError(:,i) = x(:,i+1) - T(1:3,4);                               % For plotting
                angleError(:,i) = deltaTheta;
                
                %                 plot3(x(1,:),x(2,:),x(3,:),'k.','LineWidth',1) % For plotting
            end
            collisionToEnvironment = self.checkcollsion(qMatrix)
            if collisionToEnvironment==0
                self.animateQ(qMatrix);
                self.commandCytonJoints(qMatrix);
            else
                disp("collision prevented");
            end
            
        end
        function initVel(self)
            self.cute_velocity_publisher = rospublisher('/cyton_velocity_commands');
            self.cute_velocity_msg = rosmessage(self.cute_velocity_publisher);
        end
        
        function commandCytonVelocity(self, qdot, deltaT)
            r = rosrate(1/deltaT);
            reset(r);
            %             for i= 1:size(qdot,1)
            for i= 1:100
                self.cute_velocity_msg.Data = qdot(i,:);
                self.cute_velocity_publisher.send(self.cute_velocity_msg);
                waitfor(r);
            end
            self.cute_velocity_msg.Data = [0 0 0 0 0 0 0];
            self.cute_velocity_publisher.send(self.cute_velocity_msg);
        end
        
        function [jointStates] = getRealJointStates(self)
            if (self.CytonFlag==1)
                receive(self.stateSub,2)
                msg = self.stateSub.LatestMessage;
                jointStates=msg.JointAngles;
            else
                jointStates = self.model.getpos();
            end
        end
        
        %%
        function shakeCup(self, joint, angle)
            angle_a = [angle -2*angle 2*angle -angle]';
            for j = 1:size(angle_a);
                q1 = self.model.getpos();
                q2 = q1;
                q2(joint) = q2(joint)+deg2rad(angle_a(j));
                steps = 2;
                s = lspb(0,1,steps);
                self.qMatrix = nan(steps,7);
                for i = 1:steps
                    qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                    [~, ~] = self.model.fkine(qMatrix(i,:));
                    %self.model.animate(qMatrix(1,:));
                    
                end
                for MoveIndex = 1:size(qMatrix,1)
                    animate(self.model,qMatrix(MoveIndex,:));
                    drawnow();
                end
                self.commandCytonJoints(qMatrix);
                
            end
            %0.174533
            %             if (self.CytonFlag == 1)
            %
            %                 %             cute_single_joint_client = rossvcclient('/MoveSingleJoint');
            %                 % cute_single_joint_msg = rosmessage(cute_single_joint_client);
            %                 cute_single_joint_msg.JointNumber = joint;% Joints 0-6
            %                 cute_single_joint_msg.Position = q2(joint);% (Rads)
            %                 cute_single_joint_client.call(cute_single_joint_msg);
            %
            %             end
            %
            
        end
        %%
        function moveOneJoint(self, joint, angle)
            
            
            q1 = self.model.getpos();
            q2 = q1;
            q2(joint) = q2(joint)+deg2rad(angle);
            steps = 2;
            s = lspb(0,1,steps);
            self.qMatrix = nan(steps,7);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [~, ~] = self.model.fkine(qMatrix(i,:));
                %self.model.animate(qMatrix(1,:));
                
            end
            collisionToEnvironment = self.checkcollsion(qMatrix)
            if collisionToEnvironment==0
                for MoveIndex = 1:size(qMatrix,1)
                    animate(self.model,qMatrix(MoveIndex,:));
                    drawnow();
                end
                
                
                self.commandCytonJoints(qMatrix);
            else
                disp("collision");
            end
            %0.174533
            %             if (self.CytonFlag == 1)
            %
            %                 %             cute_single_joint_client = rossvcclient('/MoveSingleJoint');
            %                 % cute_single_joint_msg = rosmessage(cute_single_joint_client);
            %                 cute_single_joint_msg.JointNumber = joint;% Joints 0-6
            %                 cute_single_joint_msg.Position = q2(joint);% (Rads)
            %                 cute_single_joint_client.call(cute_single_joint_msg);
            %
            %             end
            %
            
        end
        
        %% pur
        function pour(self, distanceDown, time)
            self.initVel();
            pos = self.model.getpos();
            position = self.model.fkine(pos);
            t = time;             % Total time (s)
            deltaT = 0.02;      % Control frequency
            steps = t/deltaT;   % No. of steps for simulation
            delta = 2*pi/steps; % Small angle change
            epsilon = 0.1;      % Threshold value for manipulability/Damped Least Squares
            W = diag([1 1 1 0.1 0.1 0.1]);    % Weighting matrix for the velocity vector
            
            % 1.2) Allocate array data
            m = zeros(steps,1);             % Array for Measure of Manipulability
            qdot_test = zeros(steps,7);
            qMatrix = zeros(steps,7);       % Array for joint anglesR
            qdot = zeros(steps,7);          % Array for joint velocities
            theta = zeros(3,steps);         % Array for roll-pitch-yaw angles
            x = zeros(3,steps);             % Array for x-y-z trajectory
            positionError = zeros(3,steps); % For plotting trajectory error
            angleError = zeros(3,steps);    % For plotting trajectory error
            
            % 1.3) Set up trajectory, initial pose
            s = lspb(0,1,steps);                % Trapezoidal trajectory scalar
            %             x(1,:) = 0.5;
            %             x(2,:) = 0.5;
            delta = distanceDown/steps;
            deltay=4.5*delta;
            for i=1:steps
                x(1,i) = position(1,4);   % Points in x
                x(2,i) = position(2,4)-i*delta; % Points in y
                x(3,i) = position(3,4)-i*delta;       % Points in z
                theta(1,i) = 0;                 % Roll angle
                theta(2,i) = pi;                 % Pitch angle
                theta(3,i) = 0;                 % Yaw angle
            end
            
            T = [rpy2r(theta(1,1),theta(2,1),theta(3,1)) x(:,1);zeros(1,3) 1];          % Create transformation of first point and angle
            %             q0 = zeros(1,7);                                                            % Initial guess for joint angles
            %            if i=1
            qMatrix(1,:)=pos;
            %            else
            %            qMatrix(1,:) = self.model.ikcon(T,pos);
            % Solve joint angles to achieve first waypoint
            
            % 1.4) Track the trajectory with RMRC
            for i = 1:steps-1
                T = self.model.fkine(qMatrix(i,:));                                           % Get forward transformation at current joint state
                deltaX = x(:,i+1) - T(1:3,4);                                         	% Get position error from next waypoint
                Rd = rpy2r(theta(1,i+1),theta(2,i+1),theta(3,i+1));                     % Get next RPY angles, convert to rotation matrix
                Ra = T(1:3,1:3);                                                        % Current end-effector rotation matrix
                Rdot = (1/deltaT)*(Rd - Ra);                                                % Calculate rotation matrix error
                S = Rdot*Ra';                                                           % Skew symmetric!
                linear_velocity = (1/deltaT)*deltaX;
                angular_velocity = [S(3,2);S(1,3);S(2,1)];                              % Check the structure of Skew Symmetric matrix!!
                deltaTheta = tr2rpy(Rd*Ra');                                            % Convert rotation matrix to RPY angles
                xdot = W*[linear_velocity;angular_velocity];                          	% Calculate end-effector velocity to reach next waypoint.
                J = self.model.jacob0(qMatrix(i,:));                 % Get Jacobian at current joint state
                m(i) = sqrt(det(J*J'));
                if m(i) < epsilon  % If manipulability is less than given threshold
                    lambda = (1 - m(i)/epsilon)*5E-2;
                else
                    lambda = 0;
                end
                invJ = pinv(J'*J + lambda *eye(7))*J';                                   % DLS Inverse
                qdot(i,:) = (invJ*xdot)';                                                % Solve the RMRC equation (you may need to transpose the         vector)
                
                for j = 1:7                                                                % Loop through joints 1 to 6
                    if qMatrix(i,j) + deltaT*qdot(i,j) < self.model.qlim(j,1)                     % If next joint angle is lower than joint limit...
                        qdot(i,j) = 0; % Stop the motor
                    elseif qMatrix(i,j) + deltaT*qdot(i,j) > self.model.qlim(j,2)                 % If next joint angle is greater than joint limit ...
                        qdot(i,j) = 0; % Stop the motor
                    end
                end
                qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot(i,:);                         	% Update next joint state based on joint velocities
                
                qdot_line = qdot(i,:);
                scale = max(qdot_line);
                qdot_line =qdot_line/scale;
                % qdot_test = qdot_test(i,(qdot_line/scale));
                qdot_test(i,:)=qdot_line;
                positionError(:,i) = x(:,i+1) - T(1:3,4);                               % For plotting
                angleError(:,i) = deltaTheta;
                
                plot3(x(1,:),x(2,:),x(3,:),'k.','LineWidth',1) % For plotting
            end
            self.animateQ(qMatrix);
            self.commandCytonJoints(qMatrix);
        end
        
        %%
        function pourDrink(self)
            
            q1 = self.model.getpos();
            pourMatrix = load('pourQmatrix.mat');
            q2 = pourMatrix.qMatrix(1,:);
            
            
            steps = 10;
            s = lspb(0,1,steps);
            self.qMatrix = nan(steps,7);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [~, ~] = self.model.fkine(qMatrix(i,:));
                %self.model.animate(qMatrix(1,:));
                
            end
            for MoveIndex = 1:size(qMatrix,1)
                animate(self.model,qMatrix(MoveIndex,:));
                drawnow();
            end
            self.commandCytonJoints(qMatrix);
            
            
            steps = size(pourMatrix.qMatrix);
            
            for i = 1:steps
                self.model.animate(pourMatrix.qMatrix(i,:));
                self.commandCytonJoints(pourMatrix.qMatrix(i,:));
            end
            
            
        end
        
        %%
        function move2GlobalWaypoint(self)
            q1 = self.model.getpos();
            q2 = self.mainwaypoint;
            steps = 30;
            s = lspb(0,1,steps);
            self.qMatrix = nan(steps,7);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [~, ~] = self.model.fkine(qMatrix(i,:));
                %self.model.animate(qMatrix(1,:));
                
            end
            for MoveIndex = 1:size(qMatrix,1)
                animate(self.model,qMatrix(MoveIndex,:));
                drawnow();
            end
            self.commandCytonJoints(qMatrix);
            
        end
        
        function move2ServeWaypoint(self)
            q1 = self.model.getpos();
            q2 = self.servewaypoint;
            steps = 30;
            s = lspb(0,1,steps);
            self.qMatrix = nan(steps,7);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [~, ~] = self.model.fkine(qMatrix(i,:));
                %self.model.animate(qMatrix(1,:));
                
            end
            for MoveIndex = 1:size(qMatrix,1)
                animate(self.model,qMatrix(MoveIndex,:));
                drawnow();
            end
            self.commandCytonJoints(qMatrix);
            
        end
        
        function executeOneDrink(self)
            disp('Serving drink one!')
            cup3 = transl(0.2,0.2,0.3);
            self.moveCyton(cup3);
            
        end
        
        
        function executeTwoDrink(self)
            disp('Serving drink two!')
            self.angle_qr3
            self.angle_qr2
            input('wait')
            pointa = transl(-0.16,-0.21,0.3)*trotx(pi)*trotz(-pi);
            self.moveCyton(pointa);
            self.moveOneJoint(7,-self.angle_qr3);
            self.moveRMRC(0.195,6,-self.angle_qr3);
            self.gripper(0);
            self.move2GlobalWaypoint();
            
            self.move2ServeWaypoint();
            self.moveRMRC(-0.03,3,0);
            self.pourDrink();
            self.move2ServeWaypoint();
            self.move2GlobalWaypoint();
            self.moveCyton(pointa);
            self.moveRMRC(0.17,6,-self.angle_qr3);
            self.gripper(25);
            
            self.move2GlobalWaypoint();
            pointb = transl(0.13,-0.25,0.3)*trotx(pi)*trotz(-pi);
            self.moveCyton(pointb);
            self.moveOneJoint(7,-self.angle_qr2);
            self.moveRMRC(0.195,6,-self.angle_qr2);
            self.gripper(0);
            self.move2GlobalWaypoint();
            self.move2ServeWaypoint();
            self.moveRMRC(-0.03,3,0);
            self.pourDrink();
            self.move2ServeWaypoint();
            self.move2GlobalWaypoint();
            self.moveCyton(pointb);
            self.moveRMRC(0.17,6,-self.angle_qr3);
            self.gripper(25);
            
        end
        
        
        function executeBothDrink(self)
            disp('Mixing both Drinks!')
        end
        function executeHardcode(self)
            disp('Predefined path!')
            pointa = transl(-0.16,-0.21,0.3)*trotx(pi)*trotz(-pi);
            % % pointb = transl(0.5,0.5,0);
            % pointb = transl(0,0.3,0.3)*trotx(-pi);
            %
% %             self.moveCyton(pointa);
% %             self.moveOneJoint(7,35);
% %             self.moveRMRC(0.185,6,35);
% %             self.gripper(0);
% %             self.move2GlobalWaypoint();
% %             
% %             self.move2ServeWaypoint();
% %             self.moveRMRC(-0.03,3,0);
% %             self.pourDrink();
% %             self.move2ServeWaypoint();
% %             self.move2GlobalWaypoint();
% %             self.moveCyton(pointa);
% %             self.moveRMRC(0.17,6,35);
% %             self.gripper(25);
            
            self.move2GlobalWaypoint();
            pointb = transl(0.13,-0.25,0.3)*trotx(pi)*trotz(-pi);
            self.moveCyton(pointb);
            self.moveRMRC(0.195,6,-35);
            self.gripper(0);
            self.move2GlobalWaypoint();
            self.move2ServeWaypoint();
            self.moveRMRC(-0.03,3,0);
            self.pourDrink();
            self.move2ServeWaypoint();
            self.move2GlobalWaypoint();
            self.moveCyton(pointb);
            self.moveRMRC(0.17,6,-35);
            self.gripper(25);
            
            
        end
        
        function set_data(self,goalx1,goaly1,angle_qr3,goalx3,goaly3,angle_qr2)
            self.goalx1 = goalx1
            self.goaly1 = goaly1
            self.angle_qr3 = angle_qr3
            self.goalx3 = goalx3;
            self.goaly3 = goaly3
            self.angle_qr2 = angle_qr2
        end
        
        function [collisionToEnvironment] = checkcollsion(self,qMatrix)
            [v,f,fn] = RectangularPrism([-0.175,-0.285,-0.07],[-0.125,-0.235,1]);
            [v1,f1,fn1] = RectangularPrism([-0.175,0.205,-0.07],[-0.125,0.255,1]);%right one
            [v2,f2,fn2] = RectangularPrism([-0.6,-0.4,-0.07],[0.6,0.4,-0.06]);%right one
            
            [result,qIntersect] = IsCollision(self.model,qMatrix,f,v,fn);
            [result1,qIntersect1] = IsCollision(self.model,qMatrix,f1,v1,fn1);
            [result2,qIntersect2] = IsCollision(self.model,qMatrix,f2,v2,fn2);
            if result || result1 || result2==1
                collisionToEnvironment=1
            else
                collisionToEnvironment=0
            end
        end
        
        function kill(self)
            a = self.model.fkine(self.model.getpos());
            self.moveCyton(a);
            
            
%             cute_enable_robot_client = rossvcclient('enableCyton');
%             cute_enable_robot_msg = rosmessage(cute_enable_robot_client);
%             cute_enable_robot_msg.TorqueEnable = false; %false
%             cute_enable_robot_client.call(cute_enable_robot_msg);
        end
        function kill2(self)
                     
            cute_enable_robot_client = rossvcclient('enableCyton');
            cute_enable_robot_msg = rosmessage(cute_enable_robot_client);
            cute_enable_robot_msg.TorqueEnable = false; %false
            cute_enable_robot_client.call(cute_enable_robot_msg);
        end
        
    end
    
    
    
end

