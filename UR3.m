classdef UR3 < handle
    %UR3 class uses serial link to create a UR3 robot object. It also
    %includes numerous additional functions to conviniently complete the assignment 

    properties
        % Robot model
        model;
        % Save the end effectors transform for logging
        endEffectorTr
        %> workspace
        workspace = [-2.5 2.5 -2.5 2.5 -1 2.5];
        
        %CommandUR3 flag, 1 if you want to move the real bot
        UR3Flag = 0;
        
        qMatrix;
        
        %Imported ply file data
        faceData;
        vertexData;
        data;
        
        %Point Cloud
        pointCloud;
        XReach;
        YReach;
        ZReach;
        horizontalReach;
        workspaceVolume;
        
        fixedPart;
        %> If we have a tool model which will replace the final links model, combined ply file of the tool model and the final link models
        toolModelFilename = []; % Available are: 'DabPrintNozzleTool.ply';
        toolParametersFilename = []; % Available are: 'DabPrintNozzleToolParameters.mat';
        
        %Logger
        logger;
    end
    
    methods
        %% Constructor
        function self = UR3(name, base)
            self.GetUR3Robot(name, base);
            PlotAndColourRobot(self);
            drawnow();
            self.logger = log4matlab(['logFileName ',self.model.name,'.log']);
        end
        
        %% GetUR3Robot
        % Given a name and location, create and return a UR3 robot model
        function GetUR3Robot(self, name, base)
            pause(0.001);
            
            L1 = Link('d',0.1519,   'a',0,          'alpha',pi/2,   'qlim',[-2*pi 2*pi],        'offset', 0);
            L2 = Link('d',0,        'a',-0.24365,   'alpha',0,      'qlim',[-2*pi 2*pi],        'offset', 0); % was 'offset',pi/2
            L3 = Link('d',0,        'a',-0.21325,   'alpha',0,      'qlim',[-2.51327 2.51327],  'offset', 0);
            L4 = Link('d',0.11235,  'a',0,          'alpha',pi/2,   'qlim',[-2*pi 2*pi],        'offset', 0); % was 'offset',pi/2
            L5 = Link('d',0.08535,  'a',0,          'alpha',-pi/2,  'qlim',[-2*pi 2*pi],        'offset', 0);
            L6 = Link('d',0.0819,   'a',0,          'alpha',0,      'qlim',[-2*pi 2*pi],        'offset', 0);
            
            self.model = SerialLink([L1 L2 L3 L4 L5 L6],'name',name);
            self.model.base = transl(base);
            %             self.model.plot(zeros(1,6),'workspace', self.workspace, 'scale', 0.5);
        end
        
        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available
        function PlotAndColourRobot(self)%robot,workspace)
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex + 1} ] = plyread(['UR3Link',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex + 1} = faceData;
                self.model.points{linkIndex + 1} = vertexData;
            end
            
            if ~isempty(self.toolModelFilename)
                [ faceData, vertexData, plyData{self.model.n + 1} ] = plyread(self.toolModelFilename,'tri');
                self.model.faces{self.model.n + 1} = faceData;
                self.model.points{self.model.n + 1} = vertexData;
                toolParameters = load(self.toolParametersFilename);
                self.model.tool = toolParameters.tool;
                self.model.qlim = toolParameters.qlim;
                warning('Please check the joint limits. They may be unsafe')
            end
            % Display robot
            self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end
            self.model.delay = 0;
            
            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
        
        %% PointCloud
        function PointCloud(self)
            %Sample the joint angles within the joint limits at 30 degree increments between each of the joint limits
            %Use fkine to determine the point in space for each of these poses, so that I end up with a big list of points
            stepRads = deg2rad(80);
            qlim = self.model.qlim;
            % Don't need to worry about joint 6
            pointCloudeSize = prod(floor((qlim(1:5,2)-qlim(1:5,1))/stepRads + 1));
            pointCloud = zeros(pointCloudeSize,3);
            counter = 1;
            tic
            
            for q1 = qlim(1,1):stepRads:qlim(1,2)
                for q2 = qlim(2,1):stepRads:qlim(2,2)
                    for q3 = qlim(3,1):stepRads:qlim(3,2)
                        for q4 = qlim(4,1):stepRads:qlim(4,2)
                            for q5 = qlim(5,1):stepRads:qlim(5,2)
                                % Don't need to worry about joint 6, just assume it=0
                                q6 = 0;
                                %                     for q6 = qlim(6,1):stepRads:qlim(6,2)
                                q = [q1,q2,q3,q4,q5,q6];
                                tr = self.model.fkine(q);
                                self.pointCloud(counter,:) = tr(1:3,4)';
                                counter = counter + 1;
                                if mod(counter/pointCloudeSize * 100,1) == 0
                                    disp(['After ',num2str(toc),' seconds, completed ',num2str(counter/pointCloudeSize * 100),'% of poses']);
                                end
                                %                     end
                            end
                        end
                    end
                end
            end
            
            %Extract Max/Min values to calculate volume
            maxX = max(self.pointCloud(:,1) - self.model.base(1,4));
            minX = min(self.pointCloud(:,1) - self.model.base(1,4));
            maxY = max(self.pointCloud(:,2) - self.model.base(2,4));
            minY = min(self.pointCloud(:,2) - self.model.base(2,4));
            maxZ = max(self.pointCloud(:,3) - self.model.base(3,4));
            minZ = min(self.pointCloud(:,3) - self.model.base(3,4));
            self.XReach = max(maxX, abs(minX));
            self.YReach = max(maxY, abs(minY));
            self.ZReach = max(maxZ, abs(minZ));
            self.horizontalReach = max (self.XReach, self.YReach);
            disp('Max horizontal reach (m): ');
            disp(self.horizontalReach);
            disp('Max vertical reach (m): ');
            disp(self.ZReach);
            %Volume of ellipsoid 4/3*pi*a*b*c where a b and c are x y and z
            %dimentions, however as bas can rotate we use horizontal reach
            %twice
            self.workspaceVolume = 4/3*pi*self.horizontalReach^2*self.ZReach;
            fprintf('Workspace Volume: (m^3) %d\n', self.workspaceVolume)
            
        end
        %% Move the robot to a goal via a waypoint
        function Move(self, goal, waypoint)
            
            q1 = self.model.getpos();
            q2 = self.model.ikcon(waypoint, q1);
            steps = 50;
            s = lspb(0,1,steps);
            self.qMatrix = nan(steps,6);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [self.endEffectorTr, ~] = self.model.fkine(qMatrix(i,:));
                self.model.animate(qMatrix(1,:));
                
            end
            for MoveIndex = 1:size(qMatrix,1)
                animate(self.model,qMatrix(MoveIndex,:));
                drawnow();
            end
            self.CommandUR3(qMatrix);
            fprintf('%s end effector is now at: \n',self.model.name);
            disp(self.endEffectorTr)
            q1 = self.model.getpos();
            q2 = self.model.ikcon(goal, q1);
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [~, ~] = self.model.fkine(qMatrix(i,:));
                %self.model.animate(qMatrix(1,:));
                
            end
            for MoveIndex = 1:size(qMatrix,1)
                animate(self.model,qMatrix(MoveIndex,:));
                drawnow();
            end
            self.CommandUR3(qMatrix);
            
            self.logTransform(self.endEffectorTr)
        end
        %% Carry an object to a goal via a waypoint
        % Commented code is responsible for aligning parts to the end
        % effector, decided to not use it until next assignment if needed. 
        function Carry(self, part, goal, waypoint)

            q1 = self.model.getpos();
            q2 = self.model.ikcon(waypoint, q1);
            steps = 50;
            s = lspb(0,1,steps);
            qMatrix = nan(steps,6);
            
            for i = 1:steps
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q2;
                [self.endEffectorTr, ~] = self.model.fkine(qMatrix(i,:));
                %                 R=[self.endEffectorTr(1,1:3);self.endEffectorTr(2,1:3);self.endEffectorTr(3,1:3)];
                %                 rpy = tr2rpy(R);
                %                 Pose = eye(4);
                
                try
                    delete(part.object);
                end
                %                 part = trisurf(face,vertex(:,1),vertex(:,2),vertex(:,3));
                %                 forwardTR = makehgtform('translate',[self.endEffectorTr(1,4),self.endEffectorTr(2,4),self.endEffectorTr(3,4)]);
                %                 randRotateTRX = makehgtform('xrotate',rpy(1));
                %                 randRotateTRY = makehgtform('yrotate',rpy(2));
                %                 randRotateTRZ = makehgtform('zrotate',rpy(3));
                %                 Pose = Pose * forwardTR * randRotateTRX*randRotateTRY*randRotateTRZ;
                %                 updatedPoints = [Pose * [vertex,ones(points,1)]']'
                %                 part.Vertices = updatedPoints(:,1:3);
                
                part.object = trisurf(part.faceData,part.vertexData(:,1)+self.endEffectorTr(1,4),part.vertexData(:,2)+self.endEffectorTr(2,4),part.vertexData(:,3)+self.endEffectorTr(3,4));
                %                 part = trisurf(Tri3,Pts3(:,1)+self.endEffectorTr(1,4),Pts3(:,2)+self.endEffectorTr(2,4),Pts3(:,3)+self.endEffectorTr(3,4));
                animate(self.model,qMatrix(i,:));
                drawnow();
                %                 try
                %                     delete(part.object);
                %                 end
            end
            self.CommandUR3(qMatrix);
            q1 = self.model.getpos();
            q3 = self.model.ikcon(goal, q1);
            s = lspb(0,1,steps);
            qMatrix = nan(steps,6);
            
            for i = 1:steps
                %                 Pose = eye(4);
                try
                    delete(part.object);
                end
                qMatrix(i,:) = (1-s(i))*q1 + s(i)*q3;
                
                self.model.animate(qMatrix(1,:));
                [self.endEffectorTr,~]=self.model.fkine(qMatrix(i,:));
                %                 R=[self.endEffectorTr(1,1:3);self.endEffectorTr(2,1:3);self.endEffectorTr(3,1:3)];
                %                 rpy = tr2rpy(R);
                %
                %                 forwardTR = makehgtform('translate',[self.endEffectorTr(1,4),self.endEffectorTr(2,4),self.endEffectorTr(3,4)]);
                %                 randRotateTRX = makehgtform('xrotate',rpy(1));
                %                 randRotateTRY = makehgtform('yrotate',rpy(2));
                %                 randRotateTRZ = makehgtform('zrotate',rpy(3));
                %                 Pose = Pose * forwardTR * randRotateTRX*randRotateTRY*randRotateTRZ;
                %                 updatedPoints = [Pose * [part.vertexData,ones(points,1)]']';
                %                 part.vertexData = updatedPoints(:,1:3);
                %                 part.object = trisurf(part.faceData,part.vertexData(:,1),part.vertexData(:,2),part.vertexData(:,3));
                part.object = trisurf(part.faceData,part.vertexData(:,1)+self.endEffectorTr(1,4),part.vertexData(:,2)+self.endEffectorTr(2,4),part.vertexData(:,3)+self.endEffectorTr(3,4));
                %                 part = trisurf(Tri3,Pts3(:,1)+self.endEffectorTr(1,4),Pts3(:,2)+self.endEffectorTr(2,4),Pts3(:,3)+self.endEffectorTr(3,4));
                animate(self.model,qMatrix(i,:));
                
                drawnow();
                %                 try
                %                     delete(part.object);
                %                 end
            end
            self.CommandUR3(qMatrix);
            
            %                 fixedPart = trisurf(Tri3,Pts3(:,1)+self.endEffectorTr(1,4),Pts3(:,2)+self.endEffectorTr(2,4),Pts3(:,3)+self.endEffectorTr(3,4));
            
            
            self.logTransform(self.endEffectorTr)
        end
        %% Log Transform
        function logTransform(self, transform)
            self.logger.mlog = {self.logger.DEBUG, 'Movement', ['The endeffector transform is', self.logger.MatrixToString(transform)]};
        end
        
        %% Command real UR3
        
        function UR3on(self)
            self.UR3Flag = 1;
        end
        function UR3off(self)
            self.UR3Flag = 0;
        end
        %Given a matrix of join angles, give the UR3 every 5th row, wait
        %for user input once it has iterated through all points
        function CommandUR3(self,q)
            if(self.UR3Flag == 1)
                ur_move_joint_client = rossvcclient('/ur_move');
                ur_move_joint_msg = rosmessage(ur_move_joint_client);
                for i=1:5:size(q)
                    ur_move_joint_msg.Q = q(i,:);
                    ur_move_joint_client.call(ur_move_joint_msg);
                    
                end
                input('press enter to continue')
            end
            
            
            return;
        end
        %% Provided a ros bag attempt to animate it on a UR3
        function playROSBag(self,bagName)
            rosinit
            bag = rosbag(bagName);
            bSel = bag.select('Topic','/joint_states');
            jointmsg = readMessages(bSel,'DataFormat','struct');
            jointSize = size(jointmsg);
            
            for i = 1:1:jointSize
                q(i,:) = jointmsg{i}.Position(:,1);
            end
            for i=1:10:jointSize
                self.model.animate(q(i,:));
                pause(0.01);
            end
            disp('Finished');
        
    end
end

end