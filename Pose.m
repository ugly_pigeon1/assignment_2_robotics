%first do webcamlist
function  [goalx1,goaly1,angle_qr3,goalx3,goaly3,angle_qr2]=Pose()
% clear all
HDcam = webcam('USB 2.0 Camera: HD USB Camera');
cameraParams=load('HDcameraParams');
qr1=imread('qr1.png');
qr2=imread('qr2.jpg');
qr3=imread('qr3.png');
%%

img = HDcam.snapshot();

%     inputImage=imagesc(imageMatrix);
%     imwrite(imageMatrix, 'detect.png');
%     img=imread('detect.png');
% [x1,y1,x2,y2,x3,y3]=test3qr(img);
x1=0;
y1=0;
x2=0;
y2=0;
x3=0;
y3=0;

% y1=1080-y1;
% y2=1080-y2;
% y3=1080-y3;
% [x,y]=qrtoolboxtest(img);
basePixelx=x2;
basePixely=y2+220;

gainx1=0.61+0.008*(abs(x1-960)/20);
gainx3=0.61+0.008*(abs(x3-960)/20);

detalx1=-(x1-basePixelx)*gainx1;%x1-basePixelx is negative in mm
detaly1=(y1-basePixely)*0.6231%y1-basePixely is negative in mm

detalx3=-(x3-basePixelx)*gainx3;
detaly3=(y3-basePixely)*0.622;



%test3qr(img);
%x;
%y;

[t1,angle1]=getPoseAR(qr1,img);%base=around 0
[t2,angle2]= getPoseAR(qr2,img);
[t3,angle3]= getPoseAR(qr3,img);
%qr2 in x3 zoom use angle2 and x3,y3
goalx3 = (detalx3 - 115*sin(deg2rad(angle2(3))))/1000;
goaly3 = (detaly3 - 115*cos(deg2rad(angle2(3))))/1000;
goaly3=goaly3+0.254
goalx1 = (detalx1 - 115*sin(deg2rad(angle3(3))))/1000;
goaly1 = (detaly1 - 115*cos(deg2rad(angle3(3))))/1000;

angle_qr2=angle2(3);
angle_qr3=angle3(3);
if angle_qr2<0
    angle_qr2=(angle_qr2+180);
elseif angle_qr2>0
    angle_qr2=(angle_qr2-180);
end

if angle_qr3<0
    angle_qr3=(angle_qr3+180);
elseif angle_qr3>0
    angle_qr3=(angle_qr3-180);
end

end

% goalx1 = detalx1 - 125*sin(deg2rad(angle2(3)))
% goaly1 = detaly1 - 125*cos(deg2rad(angle2(3)))








% px=cameraParams.HDcameraParams.PrincipalPoint(1); %Principal point X
% py=cameraParams.HDcameraParams.PrincipalPoint(2); %Principal point Y
% fx=cameraParams.HDcameraParams.FocalLength(1);
% fy=cameraParams.HDcameraParams.FocalLength(2);
% z=660;
% 
% w_x1=((x1-px)*z)/fx;
% w_y1=((y1-py)*z)/fy;
% 
% w_x2=((x2-px)*z)/fx;
% w_y2=((y2-py)*z)/fy;
% 
% w_x3=((x3-px)*z)/fx;
% w_y3=((y3-py)*z)/fy;
% 
% translation = transl(0,0,660);
% rotation = troty(pi)*trotz(-pi/2);
% % rotation = trotx(pi)*trotz(pi);
% trans_base_cam = translation * rotation;
% trans_cam_point1 = transl(w_x1,w_y1,z)
% trans_cam_point2 = transl(w_x2,w_y2,z);
% trans_cam_point3 = transl(w_x3,w_y3,z);
% 
% base2point1 = trans_base_cam * (trans_cam_point1)
% base2point2 = trans_base_cam * (trans_cam_point2)
% base2point3 = trans_base_cam * (trans_cam_point3)
% 
% newWorldPoints = pointsToWorld(intrinsics,rotation,translation,imagePoints);














%  y1=1080-y1;
%  y2=1080-y2;
%  y3=1080-y3;
% %         [x,y]=qrtoolboxtest(img);
%         basePixelx=x2;
%         basePixely=y2-230.16;
%
%         gainx1=0.61+0.007*(abs(y1-650)/10);
%         gainx3=0.61+0.007*(abs(y3-650)/10);
%
%         detalx1=-(x1-basePixelx)*gainx1;
%         detaly1=(y1-basePixely)*0.63;
%
%         detalx3=-(x3-basePixelx)*gainx3;
%         detaly3=(y3-basePixely)*0.63;
%
%
%
% %         test3qr(img);
% %         x;
% %         y;
%
%         [t1,angle1]=getPoseAR(qr1,img);%base=around 0
%         [t2,angle2]= getPoseAR(qr2,img);
%         [t3,angle3]= getPoseAR(qr2,img);
%         goalx2 = 0;
%         goaly2 = 0;
%
%         goalx1 = detalx1 + 125*sin(deg2rad(angle2(3)))
%         goaly1 = detaly1 + 125*cos(deg2rad(angle2(3)))
%
%         leftgoalx1 = detalx1 + 125*sin(deg2rad(angle2(3)))
%         leftgoaly1 = detaly1 + 125*cos(deg2rad(angle2(3)))
%
%         goalx3 = detalx3 + 125*sin(deg2rad(angle3(3)))
%         goaly3 = detaly3 + 125*cos(deg2rad(angle3(3)))
%
% %         angle2=(180-angle2+90)*
%         %%
% %         px=cameraParams.HDcameraParams.PrincipalPoint(1); %Principal point X
% %         py=cameraParams.HDcameraParams.PrincipalPoint(2); %Principal point Y
% %         fx=cameraParams.HDcameraParams.FocalLength(1);
% %         fy=cameraParams.HDcameraParams.FocalLength(2);
% %         z=0.6;
% %         w_x=(x-px)*z/fx
% %         w_y=(y-py)*z/fy
%
%
% %% Claibration
% % for i=20:24
% % pause(3);
% % imageMatrix = HDcam.snapshot();
% % inputImage=imagesc(imageMatrix)
% % imwrite(imageMatrix, [num2str(i),'.png']);
% % imshow([num2str(i),'.png']);
% % end
%
%
% %first do webcamlist
% % clear all
% % HDcam = webcam('HD USB Camera');
% % cameraParams=load('HDcameraParams');
% % %%
% % while(1)
% %     imageMatrix = HDcam.snapshot();
% %     inputImage=imagesc(imageMatrix);
% %     imwrite(imageMatrix, 'detect.png');
% %     img=imread('detect.png');
% %     try
% %         [x,y]=qrtoolboxtest(img);
% % %         x;
% % %         y;
% %         imwrite(img, 'img.png');
% %         %getPoseAR()
% %         %%
% %         px=cameraParams.HDcameraParams.PrincipalPoint(1); %Principal point X
% %         py=cameraParams.HDcameraParams.PrincipalPoint(2); %Principal point Y
% %         fx=cameraParams.HDcameraParams.FocalLength(1);
% %         fy=cameraParams.HDcameraParams.FocalLength(2);
% %         z=0.6;
% %         w_x=(x-px)*z/fx
% %         w_y=(y-py)*z/fy
% %         %%
% %     catch
% %         disp('Error')
% %
% %     end
% %
% %     pause(3)
% % end
%
% %% Claibration
% % for i=20:24
% % pause(3);
% % imageMatrix = HDcam.snapshot();
% % inputImage=imagesc(imageMatrix)
% % imwrite(imageMatrix, [num2str(i),'.png']);
% % imshow([num2str(i),'.png']);
% % end
%
%
%
%
