clear all;

robot1Base = [0,0,0];
Cyton=Cyton('robot1', robot1Base)
hold on;
%Cyton.model.teach()
%%
% Create image target (points in the image plane) 
pStar = [662 362 362 662; 362 362 662 662];

%Create 3D points
% P=[1.8,1.8,1.8,1.8;
% -0.25,0.25,0.25,-0.25;
%  1.25,1.25,0.75,0.75];
P=[-0.25,0.25,0.25,-0.25;
-0.2,-0.2,-0.2,-0.2;
 0,0,0.25,0.25];

% Add the camera
cam = CentralCamera('focal', 0.08, 'pixel', 10e-5, ...
'resolution', [1024 1024], 'centre', [512 512],'name', 'UR10camera');

% frame rate
fps = 25;

%Define values
%gain of the controler
lambda = 0.6;
%depth of the IBVS
depth = mean (P(1,:));

%% 1.2 Initialise Simulation (Display in 3D)

%Display UR10
% Tc0= r.model.fkine(q0);
Tc0=transl(0.2,0.2,0)*trotx(pi/2)
% r.model.animate(q0');
drawnow

% plot camera and points
cam.T = Tc0;

% Display points in 3D and the camera
cam.plot_camera('Tcam',Tc0, 'label','scale',0.1);
plot_sphere(P, 0.05, 'b')
lighting gouraud
light

%% 1.3 Initialise Simulation (Display in Image view)

%Project points to the image
p = cam.plot(P, 'Tcam', Tc0);

%camera view and plotting
cam.clf()
cam.plot(pStar, '*'); % create the camera view
cam.hold(true);
cam.plot(P, 'Tcam', Tc0, 'o'); % create the camera view
pause(2)
cam.hold(true);
cam.plot(P);    % show initial view


%Initialise display arrays
vel_p = [];
uv_p = [];
history = [];

